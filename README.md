# Walter's Selfbot (dockerized by mcfan4256)
Please do NOT whine to me if you got tos'd, this is against discord ToS!

PS: You need to edit the ENV

if there was errors, pull a request to fix it, easy.


# Installation Guide

* Prerequisite setup
  * Arch: `sudo pacman -S docker git`
  * Ubuntu/Debian: `sudo apt install docker git`
  * Fedora/RHEL: `sudo dnf install docker git`
  * Windows: (Not a windows user anymore, arch user btw)

* Clone from github
  * `git clone https://lab.com/mcfan4256/walterselfbot.git`
  * `cd walterselfbot`

* Read the code
  * Open your favorite editor and read it to make sure I am not stealing your token

* Run Docker container
  * `sudo docker run -d -e TOKEN=[YOUR TOKEN GOES HERE] -e PREFIX=[YOUR PREFIX] --name walterselfbot registry.gitlab.com/mcfan4256/walterselfbot/main:latest`
  * you need to edit `[YOUR TOKEN GOES HERE]` and `[YOUR PREFIX]`
  * To change other settings, check envs.md for info
* Stop container
  * `sudo docker container stop walterselfbot`
